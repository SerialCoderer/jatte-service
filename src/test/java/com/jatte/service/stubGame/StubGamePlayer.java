package com.jatte.service.stubGame;




import com.jatte.api.cards.Card;
import com.jatte.api.cards.Suits;
import com.jatte.client.GamePlayer;

import java.util.List;

/**
 * Created by Jovaughn Lockridge on 9/17/16.
 */
public class StubGamePlayer extends GamePlayer {

    private String userName;
    public StubGamePlayer(String userName){
        this.userName = userName;
    }

    @Override
    public String getUserName() {
        return this.userName;
    }

    public StubGamePlayer() {
        super();
    }

    @Override
    public List<Card<Suits, Integer>> getPlayerCards() {
        return null;
    }

    @Override
    public Iterable<Card<Suits, Integer>> playerCards() {
        return null;
    }
}
