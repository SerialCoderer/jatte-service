package com.jatte.service.stubGame;

import com.jatte.BoardGame;
import com.jatte.PlayerMove;
import com.jatte.api.cards.Card;
import com.jatte.api.cards.Suits;
import com.jatte.client.GameStateUpdate;
import java.io.Serializable;
import java.util.Map;


/**
 * Created by Jovaughn Lockridge on 9/17/16.
 */
public class StubGame extends BoardGame<Integer, StubGamePlayer> implements Serializable {


    private boolean isFull;


    @Override
    public void endGame() {

    }

    @Override
    public GameStateUpdate startGame() {
        return null;

    }

    @Override
    public String getName() {
        return "STUBGAME";
    }


    @Override
    public GameStateUpdate move(Integer move){
        return null;
    }

    @Override
    public boolean isGameOver() {
        return false;
    }

    @Override
    public void addPlayer(String userName) {
        StubGamePlayer player = new StubGamePlayer(userName);
        super.addPlayer(player);
        this.isFull = true;
    }

    @Override
    public boolean isFull() {
        return this.isFull;
    }

    public Map<String, Card<Suits, Integer>> getBoardTable() {
        return null;
    }

    public GameStateUpdate move(PlayerMove playerMove) {
        return null;
    }



}
