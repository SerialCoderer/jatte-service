package com.jatte.service;

import com.jatte.service.serverRequest.ServerActions;
import com.jatte.service.serverRequest.ServerAuthenticationRequest;
import com.jatte.service.serverRequest.ServerRequest;
import com.jatte.service.serverRequest.GameNetworkResponse;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.io.*;
import java.util.UUID;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Jovaughn Lockridge on 6/27/16.
 */
public class ServerThreadTest {


    private AuthenticationService authService = mock(AuthenticationService.class);
    private FileOutputStream out = null;
    private ObjectOutputStream oout = null;

    private ObjectInputStream ois = null;

    private FileOutputStream testOut = null;
    private ObjectOutputStream outter = null;
    private ObjectInputStream testIn = null;
    private ServerManager stm = mock(ServerManager.class);
    private GameStateObservable gameStateObservable;

    @Before
    public void setUp() throws IOException {

        gameStateObservable = new GameStateObservable(stm);
        out = new FileOutputStream("toTestEnv.txt");
        oout = new ObjectOutputStream(out);

        //when(socket.getOutputStream()).thenReturn(oout);

        testIn = new ObjectInputStream(new FileInputStream("toTestEnv.txt"));

        testOut = new FileOutputStream("toServer.txt");
        outter = new ObjectOutputStream(testOut);

        ois = new ObjectInputStream(new FileInputStream("toServer.txt"));
        //when(socket.getInputStream()).thenReturn(ois);
    }


    @Test
    @Ignore
    public void serverShouldCallAuthenticationServiceWhenReceivingAuthenicationRequest() {

    }

    @Test
    @Ignore
    public void successfulAuthenticationShouldSend_LOGIN_SUCCESSFUL() {

    }


    @Test
    @Ignore
    public void unSuccessfulAuthenticationShouldSend_LOGIN_UNSUCCESSFUL() {

    }

    private void sendRequestPayLoad(ServerRequest request) {
        try {
            outter.writeObject(request);
            outter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
