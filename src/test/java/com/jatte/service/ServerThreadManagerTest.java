package com.jatte.service;


import com.jatte.service.serverRequest.ServerAuthenticationRequest;
import com.jatte.service.serverRequest.ServerRequest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.*;
import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Jovaughn Lockridge on 6/27/16.
 */
public class ServerThreadManagerTest {


    private AuthenticationService authService = mock(AuthenticationService.class);
    private FileOutputStream out = null;
    private ObjectOutputStream oout = null;
    private ObjectInputStream ois = null;
    private ServerManager stm = mock(ServerManager.class);
    private GameStateObservable gameStateObservable = new GameStateObservable(stm);

    @Before
    public void setUp() throws IOException {
        out = new FileOutputStream("test.txt");
        oout = new ObjectOutputStream(out);
        ois = new ObjectInputStream(new FileInputStream("test.txt"));
    }


    @Test
    @Ignore
    public void serverShouldCallAuthenticationServiceWhenReceivingAuthenicationRequest() {
//        try {
//            sendRequestPayLoad(new ServerAuthenticationRequest("jova", "lol"));
//            Server st = new Server(UUID.randomUUID(), stm, gameStateObservable);
//            //  when(socket.getInputStream()).thenReturn(ois);
//            when(socket.isConnected()).thenReturn(true);
//            st.init(socket);
//            st.start();
//            Thread.sleep(1000);
//            verify(authService).authenicate("jova", "lol");
//
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    private void sendRequestPayLoad(ServerRequest request) {
        try {
            oout.writeObject(request);
            oout.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
