package com.jatte.service;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import com.jatte.Player;
import org.junit.Before;
import org.junit.Test;


public class ServerManagerTest {
	
	private StubOutputStream sos = new StubOutputStream();
	private StubInputStream sis = new StubInputStream();
	private OutputStream os = mock(OutputStream.class);
	private InputStream is = mock(InputStream.class);
	private AuthenticationService authService = mock(AuthenticationService.class);
	private ServerManager stm = mock(ServerManager.class);
	private Player player1;
	private Player player2;
	private Player player3;
	private Player player4;
	private UUID uuid1 = UUID.randomUUID();
	private UUID uuid2 = UUID.randomUUID();
	private UUID uuid3 = UUID.randomUUID();
	private UUID uuid4 = UUID.randomUUID();
	private GameStateObservable gameStateObservable;

	@Before
	public void start(){

		//-Djava.ext.dirs=/Users/Jovaughn Lockridge/jars
		System.setProperty("java.ext.dirs", "/Users/Jovaughn Lockridge/jars" );
		gameStateObservable = new GameStateObservable(stm);
		player1 = new Player(uuid1, "tester1");
		player2 = new Player(uuid2, "tester2");
		player3 = new Player(uuid3, "tester3");
		player4 = new Player(uuid4, "tester4");
	}

	@Test
	public void shouldConnect() {

	}
	
	@Test
	public void testCreateGame(){

//		ServerManager stm1 = mock(ServerManager.class);
//		Server s1 = new Server(uuid1, stm1, gameStateObservable);
////		s1.joinGame("STUBGAME", "tester1");
//
//		ArgumentCaptor<GameNetworkResponse> networkResponseCaptor = ArgumentCaptor.forClass(GameNetworkResponse.class);
//		verify(stm1, times(2)).sendPayLoad(any(UUID.class), networkResponseCaptor.capture());
//		assertEquals(ServerActions.JOIN_GAME_SUCCESSFUL, networkResponseCaptor.getAllValues().get(0).getAction());
//
//
//		verify(stm1, times(2)).sendPayLoad(any(UUID.class), networkResponseCaptor.capture());
//		assertEquals(ServerActions.START, networkResponseCaptor.getAllValues().get(1).getAction());
//
//		BoardGame g = networkResponseCaptor.getAllValues().get(1).getData();
//
//		Assert.assertNotNull(g.getPlayer("tester1"));

	}

	@Test
	public void testFindOpenGame(){
//		Server.createGame("com.jcomm.game.heartsgame.HeartsGame", "");
//		BoardGame g = Server.findOpenGame("com.jcomm.game.heartsgame.HeartsGame");
//		Assert.assertFalse(g.isFull());
//		g = Server.findOpenGame("com.jcomm.game.heartsgame.HeartsGame");
//		Assert.assertNull(g);
	}
	
	@Test
	public void testGetGameByGameKey(){
		
	}
}
