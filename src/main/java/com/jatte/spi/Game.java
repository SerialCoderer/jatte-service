/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jatte.spi;


import com.jatte.GameKey;
import com.jatte.client.GameStateUpdate;
import com.jatte.service.GameStateObservable;


/**
 * @author Jovaughn Lockridge
 */
public interface Game<T> {
    boolean isGameOver();

    void endGame();

    GameStateUpdate startGame();

    void setNumOfPlayersToPlay(int countOfPlayers);

    GameStateUpdate move(T moves);

    enum GameState {START_GAME, START_OF_ROUND, ROUND_PLAYING, END_OF_ROUND, END_OF_GAME, PAUSE}

    String getName();

    String getGameMessage();

    void setGameKey(GameKey gameKey);

    GameKey getGameKey();

    void setGameStateObservable(GameStateObservable o);
}
