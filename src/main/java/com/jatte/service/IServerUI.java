package com.jatte.service;

public interface IServerUI {
	
	void submit();
	
	String getPortNumber();
	void setPortNumber(String portnumber);

}
