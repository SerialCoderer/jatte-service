package com.jatte.service;

import com.jatte.BoardGame;
import com.jatte.GameKey;
import com.jatte.PlayerMove;

import com.jatte.api.cards.Card;
import com.jatte.api.cards.Suits;
import com.jatte.client.GamePlayer;
import com.jatte.client.GameStateUpdate;
import com.jatte.proto.Jatte;
import com.jatte.service.serverRequest.*;
import com.jatte.spi.Game;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Server extends Thread {

    private static final Logger LOGGER = LoggerFactory.getLogger(Server.class);

    private UUID uuid;
    private GameKey gameKey;
    protected boolean isStillConnected;
    private boolean isAuthenticated = false;
    private static AtomicInteger gameKeyUuid = new AtomicInteger(0);
    private static Map<String, Map<GameKey, BoardGame<?, ?>>> mapOfMapGames = new ConcurrentHashMap<>();
    private static Map<String, UUID> userNamesToUUIDs = new ConcurrentHashMap<>();
    private final ServerManager stm;
    private final String userName;
    private GameStateObservable gameStateObservable;

    /**
     * Creates a Server client object that will be playing
     * a game on the server
     *
     * @param uuid uniquely identifies the client on the server
     */
    public Server(UUID uuid, ServerManager stm, GameStateObservable gameStateObservable, String userName) {
        this.uuid = uuid;
        this.stm = stm;
        this.gameStateObservable = gameStateObservable;
        this.userName = userName;
    }

    /**
     * The run method accepts a AbstractNetworkObject objects sent from the
     * server method performs the specified operations. Following is a list of
     * commands
     */
    @Override
    public void run() {
        while (isStillConnected) {
            ///TODO: Temporary until I get the service running communicating with clients
        }
    }

    public void login(String userName) {
        userNamesToUUIDs.put(userName, uuid);
    }

    public Stream<Jatte.GameUpdateReply> gameAction(ServerActions serverAction, GameServerAction gameServerAction) {
        LOGGER.info("Received request={}, from client={} ", serverAction, gameServerAction);
        switch (serverAction) {
            case CREATE_USER:
                break;
            case LOGIN:
                break;
            case JOIN_GAME:
                return joinGame(gameServerAction);
            case LOGIN_SUCCESSFUL:
                break;
            case LOGIN_UNSUCCESSFUL:
                break;
            case CONNECTION_INITIATED:
                break;
            case CONNECT:
                break;
            case MOVE:
                return move(gameServerAction);
            case CLIENT_ACTION:
                break;
            case START:
                break;
            case MESSAGE:
                break;
            case UPDATE_SCREEN:
                break;
            case UPDATE_BOARD:
                break;
            case UPDATE_SCORE:
                break;
            case GAME_INTERRUPTED:
                break;
            case GAME_KEY_SERVER_KEY:
                break;
        }
        return Stream.empty();
    }

    public Stream<Jatte.GameUpdateReply> move(GameServerAction gameServerAction) {
        PlayerMove playerMove = gameServerAction.getPlayerMove();
        BoardGame<?, ?> g = getGame(this.gameKey);

        if (g.getGameMessage() != null) {
            sendMessageToAllInGame(g);
        }

        g.move(playerMove);
        sendUpdateToAllInGame(g);

        if (g.isGameOver()) {
            g.endGame();
            sendUpdateToAllInGame(g);
        } else {

            if (g.getGameMessage() != null) {
                sendMessageToAllInGame(g);
            }

            if (g.isPostMoveUpdate()) {
                g.postMove();
                sendUpdateToAllInGame(g);
            }
            if (g.getGameMessage() != null) {
                sendMessageToAllInGame(g);
            }
        }
        List<Jatte.GameUpdateReply> gameStateUpdate = g.getGamePlayersMap()
                .keySet()
                .stream()
                .map(l -> transformGame(g, ServerActions.GAME_STATUS_CHANGE, userNamesToUUIDs.get(l), ""))
                .collect(Collectors.toList());
        return gameStateUpdate.stream();
    }

    /**
     * This method checks to see if the user has been authenticated.
     * It will block for 5 seconds, giving the user time to attempt an authentication.
     * before timing out.
     *
     * @return authenication status
     */
    public boolean isAuthenticated() {
        return isAuthenticated(5);
    }


    /**
     * This method checks to see if the user has been authenticated.
     * It will block for amount of seconds passed in, giving the user time to attempt an authentication.
     * before timing out.
     *
     * @param secondsToWait timeout period before returning the authentication status
     * @return authenication status
     */
    public boolean isAuthenticated(long secondsToWait) {
        return isAuthenticated;
    }


    public Stream<Jatte.GameUpdateReply> joinGame(GameServerAction gameServerAction) {
        String gameName = gameServerAction.getGameName();
        BoardGame<?, ?> game = findOpenGame(gameName);
        if (game == null) {
            game = createGame(gameName);
        }
        this.gameKey = game.getGameKey();
        String userName = userNamesToUUIDs
                .entrySet().stream()
                .filter((s) -> s.getValue().toString().equals(uuid.toString()))
                .findFirst()
                .get()
                .getKey();
        game.addPlayer(userName);
        List<Jatte.GameUpdateReply> gameClientNotifications = new ArrayList<>();
        gameClientNotifications.add(transformGame(game, ServerActions.JOIN_GAME_SUCCESSFUL, this.uuid, ""));
        LOGGER.info("Added user to game {}, game id={}", game, gameKey);
        if (game.getGameMessage() != null) {
            sendMessageToAllInGame(game);
        }
        if (game.isFull()) {
            GameStateUpdate gameStateUpdate = game.startGame();
            List<Jatte.GameUpdateReply> startGameNotifications = game.getGamePlayersMap()
                    .keySet()
                    .stream()
                    .map(l -> transformGameStateUpdate(gameStateUpdate, ServerActions.START, userNamesToUUIDs.get(l)))
                    .collect(Collectors.toList());

            if (game.getGameMessage() != null) {
                sendMessageToAllInGame(game);
            }
            gameClientNotifications.addAll(startGameNotifications);
        }
        return gameClientNotifications.stream();
    }

    public BoardGame<?, ?> findOpenGame(String gameName) {
        LOGGER.info("Searching for {} game", gameName);
        Map<GameKey, BoardGame<?, ?>> games = mapOfMapGames.get(gameName);
        if (games == null) {
            return null;
        }
        for (BoardGame game : games.values()) {
            if (!game.isFull())
                return game;
        }
        return null;
    }

    private BoardGame<?, ?> createGame(String gameRequest) {
        BoardGame<?, ?> game = null;
        try {
            ServiceLoader<Game> loader = ServiceLoader.load(Game.class);
            for (Game g : loader) {
                LOGGER.info("Found game {} ", g);
                if (g.getName().equalsIgnoreCase(gameRequest)) {
                    LOGGER.info("Created new game " + g);
                    game = (BoardGame) g;
                    break;
                }
            }
            if (game == null) {
                throw new RuntimeException("Unable to find game " + gameRequest);
            }
            gameKey = createNewGameKey(gameRequest);
            LOGGER.info("Player={} has gamekey={} ", this.userName, this.gameKey);
            game.setGameKey(gameKey);
            game.setGameStateObservable(this.gameStateObservable);
            Map<GameKey, BoardGame<?, ?>> mapOfGames = mapOfMapGames.get(gameRequest);
            if (mapOfGames == null) {
                mapOfGames = new ConcurrentHashMap<>();
                mapOfMapGames.put(gameRequest, mapOfGames);
                LOGGER.info("adding the game {} to server ", gameRequest);
            }
            mapOfGames.put(gameKey, game);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return game;
    }

    public static BoardGame<?, ?> getGame(GameKey gameKey) {
        Map<GameKey, BoardGame<?, ?>> map = mapOfMapGames.get(gameKey.getGame());
        if (map == null) {
            LOGGER.info("No maps found for game={}", gameKey.getGame());
        }
        return map.get(gameKey);
    }

    private static GameKey createNewGameKey(String gameName) {
        return new GameKey(gameKeyUuid.getAndIncrement(), gameName);
    }

    private void sendUpdateToAllInGame(BoardGame game) {
//        GameNetworkResponse outgoing = new GameNetworkResponse(ServerActions.UPDATE_SCREEN, "Updating Screen", game);
//        LOGGER.info("Sending game update to game {} with game key {} . ", game, game.getGameKey());
    }

    private void sendMessageToAllInGame(BoardGame game) {
        LOGGER.info("Sending message to every player in game {} ", game, game.getGameMessage());
    }

    public GameKey getGameKey() {
        return this.gameKey;
    }


    private Jatte.GameUpdateReply transformGameStateUpdate(GameStateUpdate gameStateUpdate, ServerActions serverActions, UUID uuid) {
        return Jatte.GameUpdateReply
                .newBuilder()
                .addAllPlayers(transformPlayers(gameStateUpdate.getPlayersByUserName().values()))
                .setUuid(uuid.toString())
                .setServerAction(serverActions.name())
                .setGameAction(gameStateUpdate.getClientSideAction())
                .build();
    }

    private Jatte.GameUpdateReply transformGame(BoardGame<?, ?> game, ServerActions serverActions, UUID uuid, String gameAction) {
        return Jatte.GameUpdateReply
                .newBuilder()
                .addAllPlayers(transformPlayers(game.getGamePlayers()))
                .setUuid(uuid.toString())
                .putAllPlayerMoves(transformToPlayerMoves(game.getBoardTable()))
                .setServerAction(serverActions.name())
                .setGameAction(gameAction)
                .build();
    }

    private <G extends GamePlayer> Iterable<Jatte.Player> transformPlayers(Collection<G> players) {
        List<Jatte.Player> jattePlayers = new ArrayList<>();
        Consumer<GamePlayer> collectPlayers = (p) -> jattePlayers.add(Jatte.Player
                .newBuilder()
                .addAllCards(p.getPlayerCards().stream().map(c -> transformCard(c)).collect(Collectors.toList()))
                .setUsername(p.getUserName())
                .setPlayerScore(p.getPlayerScore())
                .build());
        players.forEach(collectPlayers);
        return jattePlayers;
    }

    private Jatte.Card transformCard(Card card) {
        return Jatte.Card.newBuilder()
                .setFaceCard(card.getFace())
                .setSuit(card.getSuit().toString())
                .setValue(card.getCardValue().intValue())
                .build();
    }

    private Map<String, Jatte.PlayersMove> transformToPlayerMoves(Map<String, Card<Suits, Integer>> moves) {
        Map<String, Jatte.PlayersMove> playersMoves = new HashMap<>();
        Consumer<Map.Entry<String, Card<Suits, Integer>>> collectMoves = (e) -> {
            String userName = e.getKey();
            Jatte.PlayersMove playerMove = Jatte.PlayersMove
                    .newBuilder()
                    .addMoves(transformCard(e.getValue()))
                    .build();
            playersMoves.put(userName, playerMove);
        };
        moves.entrySet().forEach(collectMoves);
        return playersMoves;
    }

    public String getUserName() {
        return userName;
    }

    @Override
    public String toString() {
        return "Server{" +
                "uuid=" + uuid +
                ", gameKey=" + gameKey +
                ", isStillConnected=" + isStillConnected +
                ", isAuthenticated=" + isAuthenticated +
                ", userName='" + userName + '\'' +
                '}';
    }
}
