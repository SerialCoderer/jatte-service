package com.jatte.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

/**
 * Created by Jovaughn Lockridge on 6/28/16.
 */
public class AuthenticationServiceImpl implements AuthenticationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationServiceImpl.class);
    private PreparedStatement pstmt;
    private final Connection conn;
    private final String VALIDATE_USER_QUERY = "Select * from user WHERE username=? and password=?";

    public AuthenticationServiceImpl(Connection conn) {
        this.conn = conn;
        try {
            pstmt = conn.prepareStatement(VALIDATE_USER_QUERY);
        } catch (SQLException sqle) {
            LOGGER.error("Could not prepare statement {} ", VALIDATE_USER_QUERY);
            sqle.printStackTrace();
        }
    }

    @Override
    public boolean authenicate(String name, String password) {
        String userFound = null;
        try {
            pstmt.setString(1, name);
            pstmt.setString(2, password);
            ResultSet result2 = pstmt.executeQuery();
            if (result2 == null)
                return false;
            result2.next();
            userFound = result2.getString("username");
            LOGGER.info("User return from database {} ", userFound);
        } catch (SQLException se) {
            se.printStackTrace();
        }
        return userFound != null;
    }
}
