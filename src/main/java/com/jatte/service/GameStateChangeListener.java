package com.jatte.service;

/**
 * Created by Jovaughn Lockridge on 11/28/16.
 */
public interface GameStateChangeListener {

    void action();
}
