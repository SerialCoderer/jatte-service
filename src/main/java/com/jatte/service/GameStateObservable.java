package com.jatte.service;

import com.jatte.spi.Game;

import java.io.Serializable;

/**
 * Created by jovaughn lockridge on 11/29/16.
 */
public class GameStateObservable implements Serializable {
    private final transient ServerManager stm;

    public GameStateObservable(ServerManager stm) {
        this.stm = stm;
    }

    public void notify(Game game) {
        stm.sendGameStatus(game);
    }
}
