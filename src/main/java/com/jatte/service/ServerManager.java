package com.jatte.service;

import com.jatte.spi.Game;

import java.util.UUID;

/**
 * Created by jlockrid on 6/28/2016.
 */
public interface ServerManager<T extends Game>{
    void sendGameStatus(Game game);
    void removeThread(UUID uuid);
}
