package com.jatte.service;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class ServerUI  extends JFrame implements IServerUI{

    private final JTextField tf_portNumber = new JTextField(10);
    private JButton jb_startServer;
    private ServerThreadManager stm;

    public ServerUI(ServerThreadManager stm){
        this.stm = stm;
    }

    @Override
    public void submit() {
        if(!stm.isServerRunning()) {
            if (tf_portNumber.getText().isEmpty()) {
                System.out.println("using default port number");
                new Thread(() -> stm.startServer(Integer.parseInt("9001"))).start();

            } else {
                new Thread(() -> stm.startServer(Integer.parseInt(tf_portNumber.getText()))).start();
            }
        }
    }


    public void init(){
        JTextArea editorTextBoard  = new JTextArea(5, 30);
        editorTextBoard.setEditable(false);
        JScrollPane jsp = new JScrollPane(editorTextBoard);
        jb_startServer = new JButton("Starting.......");
        jb_startServer.addActionListener((Action) -> submit());
        JPanel jp_controlPanel = new JPanel();
        JLabel jl_portNumber = new JLabel("Enter a port number to run Game Engine on: ");
        jp_controlPanel.add(jl_portNumber);
        jp_controlPanel.add(tf_portNumber);
        jsp.setSize(200, 400);
        this.add(jb_startServer, BorderLayout.SOUTH);
        this.getContentPane().add(jsp, BorderLayout.NORTH);
        this.add(jp_controlPanel, BorderLayout.CENTER);
        this.setSize(400, 400);
        this.setVisible(true);
    }


    @Override
    public String getPortNumber(){	
        return tf_portNumber.getText();
    }


    @Override
    public void setPortNumber(String portnumber) {


    }
}
