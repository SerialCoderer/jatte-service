package com.jatte.service.serverRequest;


import com.jatte.spi.Game;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Jovaughn Lockridge on 6/25/16.
 */
public class GameNetworkResponse<T extends Game> implements ServerRequest, Serializable{

    private static final long serialVersionUID = 1234L;
    private ServerActions action;
    private String message;
    private T data;
    private UUID uuid;


    public GameNetworkResponse(ServerActions action, String message, UUID uuid){
        this.action = action;
        this.message = message;
        this.uuid = uuid;
    }

    public GameNetworkResponse(ServerActions action, String message, T data){
        this.action = action;
        this.message = message;
        this.data = data;
    }

    @Override
    public ServerActions getAction() {
        return action;
    }

    public String getMessage() {
        return message;
    }

    public T getData(){
        return this.data;
    }

    @Override
    public UUID getUUID() {
        return uuid;
    }

    @Override
    public String toString() {
        return "GameNetworkResponse{" +
                "action=" + action +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
