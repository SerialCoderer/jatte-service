package com.jatte.service.serverRequest;


import com.jatte.PlayerMove;

public class GameServerAction {

    private final PlayerMove playerMove;
    private final String gameName;

    public GameServerAction(String gameName, PlayerMove playerMove) {
        this.playerMove = playerMove;
        this.gameName = gameName;
    }

    public PlayerMove getPlayerMove() {
        return playerMove;
    }

    public String getGameName() {
        return gameName;
    }
}
