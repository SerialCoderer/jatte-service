package com.jatte.service.serverRequest;

import java.util.UUID;

/**
 * Created by Jovaughn Lockridge on 6/27/16.
 */
public interface ServerRequest {

    ServerActions getAction();
    UUID getUUID();
}
