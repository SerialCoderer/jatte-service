package com.jatte.service.serverRequest;
/*********************************************************************
 * Class AbstractNetworkObject
 * Description: this class is what the server and client use to communicate
 * and send data across the network
 * *
 *
 * @author Jovaughn Lockridge jovaughn@jovaapps.com
 * Copyright @ 2006
 ********************************************************************/


import com.jatte.GameKey;

import java.io.Serializable;
import java.util.UUID;



public class GameNetworkRequest<T> implements ServerRequest, Serializable, Cloneable {

    private static final long serialVersionUID = 1234L;
    private ServerActions action;
    private GameKey gameKey;
    private UUID clientUUID;
    private String message;
    private T data;


    /**
     *
     * @param action Server action
     * @param data g value what kind of data is being sent across the network
     *
     **/
    public GameNetworkRequest(ServerActions action, T data) {
        this.action = action;
        this.data = data;
    }


    /**
     * Use this constructor when you choose to send your data packet with a message
     *
     * @param action Server action
     * @param message text information that is passed to other clients
     * @param data  data that is transported to server and clients
     * @param uuid  client universal unique identifier
     *
     *
     **/
    public GameNetworkRequest(ServerActions action,  String message,  T data, UUID uuid) {
        this.clientUUID = uuid;
        this.action = action;
        this.data = data;
        this.message = message;
    }


    /**
     * Use this constructor when you choose to send your data packet without a message
     *
     * @param action Server action
     * @param data  data that is transported to server and clients
     * @param uuid client universal unique identifier
     *
     *
     **/
    public GameNetworkRequest(ServerActions action, T data, UUID uuid) {
        this.clientUUID = uuid;
        this.action = action;
        this.data = data;
    }






    /**
     *
     * @precondition:
     *    This AbstractNetworkObject object has been instantiated.
     *
     * @return:
     *    a Object of the AbstractNetworkObject
     **/
    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }
    }

    /**
     *
     * @return: a generic data passed to server
     **/
    public T getData() {
        return data;
    }


    public String getMessage(){
        return this.message;
    }


    @Override
    public ServerActions getAction() {
        return this.action;
    }

    /**
     *
     * @return:
     *    a int of the AbstractNetworkObject gameKey variable
     **/
    public GameKey getGameKey() {
        return gameKey;
    }

    @Override
    public String toString() {
        return "GameNetworkRequest{" +
            "action=" + action +
            ", data=" + data +
            ", gameKey=" + gameKey +
            ", clientUUID=" + clientUUID +
            '}';
    }

    /**
     *
     *
     *
     *
     **/
    @Override
    public UUID getUUID() {
        return clientUUID;
    }


}


	