package com.jatte.service.serverRequest;

import java.io.Serializable;

/**
 * Created by Jovaughn Lockridge on 6/25/16.
 */
public enum ServerActions implements Serializable {

    CLIENT_ACTION,
    CONNECT,
    CONNECTION_INITIATED,
    CREATE_USER,
    JOIN_GAME,
    LOGIN,
    LOGIN_SUCCESSFUL,
    LOGIN_UNSUCCESSFUL,
    START,
    MESSAGE,
    JOIN_GAME_SUCCESSFUL, UPDATE_SCREEN, UPDATE_BOARD,
    UPDATE_SCORE, GAME_INTERRUPTED, MOVE, GAME_KEY_SERVER_KEY, GAME_STATUS_CHANGE

}
