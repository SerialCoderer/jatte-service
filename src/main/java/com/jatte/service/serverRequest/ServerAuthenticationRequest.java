package com.jatte.service.serverRequest;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Jovaughn Lockridge on 6/27/16.
 */
public class ServerAuthenticationRequest implements ServerRequest, Serializable {
    private static final long serialVersionUID = 1234L;
    private ServerActions action = ServerActions.LOGIN;
    private String userName;
    private String password;


    public ServerAuthenticationRequest(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getUserName() {
        return userName;
    }

    public ServerActions getAction(){
        return action;
    }

    @Override
    public UUID getUUID() {
        return null;
    }

    @Override
    public String toString() {
        return "ServerAuthenticationRequest{" +
                "action=" + action +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
