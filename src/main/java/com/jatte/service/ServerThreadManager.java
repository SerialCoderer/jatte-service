package com.jatte.service;

/**
 * <p>
 * Description: this class host many games and keeps track of all the users that
 * are connected to each game. It also uses the UserDatabase to validate valid
 * user's to connect to the server and play games.
 *
 * @author Jovaughn Lockridge jovaughn@jovaapps.com
 * <p>
 * Copyright @ 2006
 * <p/>
 */

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.jatte.PlayerMove;
import com.jatte.api.cards.Card;
import com.jatte.api.cards.Suits;
import com.jatte.client.GamePlayer;
import com.jatte.proto.GameServiceGrpc;
import com.jatte.proto.Jatte;
import com.jatte.service.serverRequest.GameServerAction;
import com.jatte.service.serverRequest.ServerActions;
import com.jatte.spi.Game;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServerThreadManager extends Thread implements ServerManager<Game<GamePlayer>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServerThreadManager.class);
    private static ConcurrentHashMap<UUID, Server> threads = new ConcurrentHashMap<>();
    private volatile boolean canAcceptConnections;
    private io.grpc.Server server;
    private AuthenticationService authService = null;
    private GameStateObservable observable = null;
    private static ServerThreadManager serverManager = null;
    private static ConcurrentHashMap<UUID, StreamObserver<Jatte.GameUpdateReply>> clientConnections = new ConcurrentHashMap<>();


    public void sendGameStatus(Game game) {
    }


    public void removeThread(UUID uuid) {
        Server s = threads.remove(uuid);
        if (null != s) {
            LOGGER.info("Successfully removed thread {} ", uuid);
        }
    }

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class.forName("org.h2.Driver").newInstance();
        final int portNumber = 9001;
        serverManager = new ServerThreadManager();
        LOGGER.info("Please enter a port number to run the game engine on this machine (default is 9001) : ");
        try {
            serverManager.startServer(9001);
        } catch (Exception e) {
            LOGGER.error(
                    "Error with the user input. Port number={}",
                    portNumber, e);
        }
    }

    public boolean isServerRunning() {
        return canAcceptConnections;
    }

    private void stopServer() {
        if (server != null) {
            server.shutdown();
        }
    }

    public void startServer(int portNumber) {
        try {

            server = ServerBuilder.forPort(portNumber)
                    .addService(new GameServiceImpl())
                    .build()
                    .start();
        } catch (IOException e) {
            LOGGER.error("Could not server ", e);
            e.printStackTrace();
        }
        LOGGER.info("Server started, listening on " + portNumber);
        try {
            Class.forName("org.h2.Driver").newInstance();

            Connection conn = DriverManager.
                    getConnection("jdbc:h2:mem:test;" +
                            "INIT=RUNSCRIPT FROM 'classpath:datasource/jatte.sql'\\;" +
                            "RUNSCRIPT FROM 'classpath:datasource/populateJatte.sql'", "sa", "");
            authService = new AuthenticationServiceImpl(conn);
            observable = new GameStateObservable(this);
            ScheduledExecutorService scheduler =
                    Executors.newScheduledThreadPool(1);
            LOGGER.info("Starting sever on port number " + portNumber);
            scheduler.scheduleAtFixedRate(() -> {
                LOGGER.info("Count of threads {}", threads.size());
                threads.values().stream().forEach(t -> LOGGER.info("Thread {}", t));
            }, 5, 5, TimeUnit.MINUTES);
            canAcceptConnections = true;
        } catch (SQLException sqle) {
            LOGGER.error("Could not start the server", sqle);
            sqle.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        /* The port on which the server should run */

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // Use stderr here since the logger may have been reset by its JVM shutdown hook.
                System.err.println("*** shutting down gRPC server since JVM is shutting down");
                ServerThreadManager.this.stopServer();
                System.err.println("*** server shut down");
            }
        });
        String remoteHost = "";

    }

    class GameServiceImpl extends GameServiceGrpc.GameServiceImplBase {
        @Override
        public void login(Jatte.LoginRequest request, io.grpc.stub.StreamObserver<Jatte.LoginReply> responseObserver) {
            LOGGER.info("Logging in user {} ", request.getUsername());
            Jatte.LoginReply loginReply = null;
            if (authService.authenicate(request.getUsername(), request.getPassword())) {
                UUID uuid = UUID.randomUUID();
                Server player = new Server(uuid, serverManager, observable, request.getUsername());
                player.start();
                threads.put(uuid, player);
                player.login(request.getUsername());
                loginReply = Jatte.LoginReply.newBuilder()
                        .setIsLoggedIn(true)
                        .setNetworkId(uuid.toString())
                        .setMessage("Login was successful")
                        .build();
            } else {
                loginReply = Jatte.LoginReply.newBuilder()
                        .setIsLoggedIn(false)
                        .setNetworkId("")
                        .setMessage("Login was unsuccessful")
                        .build();
            }
            responseObserver.onNext(loginReply);
            responseObserver.onCompleted();
        }

        @Override
        public StreamObserver<Jatte.GameUpdateRequest> gameUpdate(final StreamObserver<Jatte.GameUpdateReply> responseObserver) {
            return new StreamObserver<Jatte.GameUpdateRequest>() {
                @Override
                public void onNext(Jatte.GameUpdateRequest gameUpateRequest) {
                    UUID uuid = UUID.fromString(gameUpateRequest.getUuid());
                    String userName = threads.get(uuid).getUserName();
                    clientConnections.putIfAbsent(uuid, responseObserver);
                    List<Card<Suits, Integer>> move = gameUpateRequest.getMoveRequest().getMoveList()
                            .stream()
                            .map(c -> transformJatteCard(c))
                            .collect(Collectors.toList());
                    String gameName = gameUpateRequest.getGameName();
                    PlayerMove playerMove = new PlayerMove(userName, move);
                    String serverAction = gameUpateRequest.getServerAction();
                    GameServerAction gameServerAction = new GameServerAction(gameName, playerMove);
                    Stream<Jatte.GameUpdateReply> gameClientNotificationStream = threads.get(uuid).gameAction(ServerActions.valueOf(serverAction), gameServerAction);
                    gameClientNotificationStream
                            .peek(id -> LOGGER.info("Sending {} to {} ", serverAction, id))
                            .forEach(notification -> clientConnections.get(UUID.fromString(notification.getUuid())).onNext(notification));
                }

                @Override
                public void onError(Throwable t) {
                    LOGGER.warn("cancelled");
                }

                @Override
                public void onCompleted() {
                    responseObserver.onCompleted();
                }
            };
        }
    }

    private Card<Suits, Integer> transformJatteCard(Jatte.Card jatteCard) {
        return new Card<>(Suits.valueOf(jatteCard.getSuit()), jatteCard.getValue(), jatteCard.getFaceCard());
    }
}
