package com.jatte.service;

/****************************************************
* The  NotLegalMove Exception is thrown
* when a Client makes a move that is invalid
*-------------------------------------------
 *@author Jovaughn Lockridge jovaughn@jovaapps.com
* Copyright @ 2006
*
******************************************************/
public class IllegalMoveException extends Exception {

	public IllegalMoveException(String message)
    {
       super(message);
    }
}